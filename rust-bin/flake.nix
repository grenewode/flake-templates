{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nmattia/naersk";

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      flake = false;
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, naersk, rust-overlay }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ (import rust-overlay) ];
        };
        rust = pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain.toml;
        naersk-lib = naersk.lib."${system}".override {
          cargo = rust;
          rustc = rust;
        };
        cargoToml = (builtins.fromTOML (builtins.readFile ./Cargo.toml));
      in rec {
        # `nix build`
        packages.${cargoToml.package.name} =
          naersk-lib.buildPackage { root = ./.; };
        packages.default = packages.${cargoToml.package.name};
        defaultPackage = packages.${cargoToml.package.name};

        # `nix run`
        apps.${cargoToml.package.name} =
          flake-utils.lib.mkApp { drv = packages.default; };
        apps.default = flake-utils.lib.mkApp { drv = packages.default; };
        defaultApp = apps.default;

        # `nix develop`
        devShell = (pkgs.mkShell { packages = [ rust ]; });
      });
}
