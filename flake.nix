{
  description = "grenewode's nix flake templates for fun and profit";

  outputs = { self, ... }: {
    templates = {
      rust-bin = {
        description = "a simple rust project template";
        path = ./rust-bin;
      };
    };
  };
}
